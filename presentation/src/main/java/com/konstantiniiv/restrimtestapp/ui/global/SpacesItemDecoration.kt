package com.konstantiniiv.restrimtestapp.ui.global

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class SpacesItemDecoration(val spaces: Int, val spancount: Int, val includeEdge: Boolean)
    : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View?,
                                parent: RecyclerView, state: RecyclerView.State?) {
        val position = parent.getChildAdapterPosition(view)
        val column = position % spancount

        if (includeEdge) {
            outRect.left = spaces - column * spaces / spancount
            outRect.right = (column + 1) * spaces / spancount
            if (position < spancount) {
                outRect.top = spaces
            }
            outRect.bottom = spaces
        } else {
            outRect.left = column * spaces / spancount
            outRect.right = spaces - (column + 1) * spaces / spancount
            if (position >= spancount) {
                outRect.top = spaces
            }
        }
    }
}