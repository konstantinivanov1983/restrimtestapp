package com.konstantiniiv.domain.cache

import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
interface MoviesCache{
    fun clear()
    fun saveAll(movies: List<MovieEntity>)
    fun getAll(): Observable<List<MovieEntity>>
    fun get(movieId: Int): Observable<Optional<MovieEntity>>
    fun isEmpty(): Observable<Boolean>
}