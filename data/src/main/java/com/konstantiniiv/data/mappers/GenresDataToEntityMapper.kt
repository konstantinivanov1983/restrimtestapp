package com.konstantiniiv.data.mappers

import com.konstantiniiv.data.entities.GenresData
import com.konstantiniiv.domain.enteties.GenresEntity

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
class GenresDataToEntityMapper {

    fun map(from: GenresData?): GenresEntity {
        return GenresEntity(name = from?.name ?: "")
    }

    fun reverseMap(from: GenresEntity): GenresData {
        return GenresData(name = from.name)
    }

    fun map(values: List<GenresData>): List<GenresEntity> {
        val returnValues = ArrayList<GenresEntity>(values.size)
        values.forEach { t: GenresData -> returnValues.add(map(t)) }
        return returnValues
    }

    fun reverseMap(values: List<GenresEntity>): List<GenresData> {
        val returnValues = ArrayList<GenresData>(values.size)
        values.forEach { t: GenresEntity -> returnValues.add(reverseMap(t)) }
        return returnValues
    }
}