package com.konstantiniiv.restrimtestapp.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.konstantiniiv.restrimtestapp.ui.global.ViewModelFactory
import com.konstantiniiv.restrimtestapp.ui.global.ViewModelKey
import com.konstantiniiv.restrimtestapp.ui.moviedetails.MovieDetailsViewModel
import com.konstantiniiv.restrimtestapp.ui.movielist.MovieListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
@Module
abstract class ViewModuleModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    internal abstract fun provideMovieListViewModel(viewModel: MovieListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailsViewModel::class)
    internal abstract fun provideMovieDetailsViewModel(viewModel: MovieDetailsViewModel): ViewModel
}