package com.konstantiniiv.domain.usecase

import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.repository.MovieRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class GetMovieListUseCase @Inject constructor(private val repository: MovieRepository) {

    fun getMovieList(): Observable<List<MovieEntity>> {
        return repository.getMovieList()
    }

}