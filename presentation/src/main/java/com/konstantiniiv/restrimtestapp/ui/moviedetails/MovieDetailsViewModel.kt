package com.konstantiniiv.restrimtestapp.ui.moviedetails

import com.jakewharton.rxrelay2.PublishRelay
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import com.konstantiniiv.domain.usecase.GetMovieUseCase
import com.konstantiniiv.restrimtestapp.Screens
import com.konstantiniiv.restrimtestapp.ui.global.BaseViewModel
import com.konstantiniiv.restrimtestapp.ui.global.ViewState
import com.konstantiniiv.restrimtestapp.ui.global.disposedBy
import com.orhanobut.logger.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieDetailsViewModel @Inject constructor(private val router: Router,
                                                private val getMovieUseCase: GetMovieUseCase)
    : BaseViewModel() {


    private val viewState = PublishRelay.create<ViewState<MovieEntity>>()

    fun observeStates() = viewState

    fun getMovie(id: Int) {
        getMovieUseCase.getMovie(id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: Optional<MovieEntity> ->
                    if (t.hasValue()) {
                        val movie = t.value
                        if (movie != null) {
                            viewState.accept(ViewState.Success(movie))
                        }
                    }
                },
                        { t: Throwable? ->
                            viewState.accept(ViewState.Error(t?.message ?: "Unknown Error"))
                        }).disposedBy(disposables)
    }

    fun onBackPressed() {
        router.backTo(Screens.FRAGMENT_MOVIES_LIST)
    }
}