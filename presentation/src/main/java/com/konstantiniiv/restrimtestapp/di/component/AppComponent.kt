package com.konstantiniiv.restrimtestapp.di.component

import com.konstantiniiv.restrimtestapp.App
import com.konstantiniiv.restrimtestapp.di.module.*
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Singleton
@Component(modules = [AppModule::class, AndroidSupportInjectionModule::class,
    ActivityModule::class, DataModule::class, FragmentModule::class, NavigationModule::class,
    ApiModule::class, MapperModule::class, ViewModuleModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}