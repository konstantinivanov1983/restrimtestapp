package com.konstantiniiv.domain.enteties

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
data class GenresEntity(var name: String = "")