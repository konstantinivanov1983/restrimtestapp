package com.konstantiniiv.restrimtestapp.ui.movielist

import com.jakewharton.rxrelay2.PublishRelay
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.usecase.GetMovieListUseCase
import com.konstantiniiv.restrimtestapp.Screens
import com.konstantiniiv.restrimtestapp.ui.global.BaseViewModel
import com.konstantiniiv.restrimtestapp.ui.global.ViewState
import com.konstantiniiv.restrimtestapp.ui.global.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieListViewModel @Inject constructor(private val router: Router,
                                             private val getMoviesListUseCase: GetMovieListUseCase)
    : BaseViewModel() {

    private val viewState = PublishRelay.create<ViewState<List<MovieEntity>>>()

    fun observeStates() = viewState

    fun init() {
        getMoviesListUseCase.getMovieList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: List<MovieEntity>? ->
                    if (t != null) viewState.accept(ViewState.Success(data = t))
                },
                        { t: Throwable? ->
                            viewState.accept(ViewState.Error(t?.message ?: "Unknown Error"))
                        })
                .disposedBy(disposables)
    }

    fun onMovieClicked(movieId: Int) {
        router.navigateTo(Screens.FRAGMENT_MOVIE, movieId)
    }

    fun onBackPressed() {
        router.exit()
    }

}