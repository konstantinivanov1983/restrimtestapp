package com.konstantiniiv.restrimtestapp.di.module

import com.konstantiniiv.data.mappers.GenresDataToEntityMapper
import com.konstantiniiv.data.mappers.MovieDataToMovieEntityMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
@Module
class MapperModule {

    @Singleton
    @Provides
    fun provideMovieMapper(genreMapper: GenresDataToEntityMapper)
            : MovieDataToMovieEntityMapper = MovieDataToMovieEntityMapper(genreMapper = genreMapper)

    @Singleton
    @Provides
    fun provideGenreMapper(): GenresDataToEntityMapper = GenresDataToEntityMapper()

}