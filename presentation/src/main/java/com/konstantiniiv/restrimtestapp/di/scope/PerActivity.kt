package com.konstantiniiv.restrimtestapp.di.scope

import javax.inject.Scope

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity