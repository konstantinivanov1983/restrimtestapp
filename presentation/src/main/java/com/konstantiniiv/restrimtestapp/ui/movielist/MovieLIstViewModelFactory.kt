package com.konstantiniiv.restrimtestapp.ui.movielist

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.konstantiniiv.domain.usecase.GetMovieListUseCase
import ru.terrakok.cicerone.Router

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieListViewModelFactory(val router: Router,
                                val getMoviesListUseCase: GetMovieListUseCase)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieListViewModel(router = router, getMoviesListUseCase = getMoviesListUseCase) as T
    }
}