package com.konstantiniiv.restrimtestapp.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.konstantiniiv.restrimtestapp.R
import com.konstantiniiv.restrimtestapp.Screens
import com.konstantiniiv.restrimtestapp.ui.global.BackButtonListener
import com.konstantiniiv.restrimtestapp.ui.moviedetails.MovieDetailsFragment
import com.konstantiniiv.restrimtestapp.ui.movielist.MovieListFragment
import dagger.android.support.DaggerAppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) router.navigateTo(Screens.FRAGMENT_MOVIES_LIST)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment != null
                && fragment is BackButtonListener
                && fragment.onBackClick()) {
            return
        } else {
            super.onBackPressed()
        }
    }

    private val navigator = object : SupportAppNavigator(this, R.id.container) {

        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? {
            return null
        }

        override fun createFragment(screenKey: String?, data: Any?): Fragment? {
            return when (screenKey) {
                Screens.FRAGMENT_MOVIES_LIST -> MovieListFragment()
                Screens.FRAGMENT_MOVIE -> {
                    if (data is Int) {
                        MovieDetailsFragment.newInstance(data)
                    } else throw IllegalArgumentException("Data should be Int")
                }
                else -> null
            }
        }

        override fun back() {
            finish()
        }

    }
}