package com.konstantiniiv.restrimtestapp.ui.moviedetails

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.konstantiniiv.domain.enteties.GenresEntity
import com.konstantiniiv.restrimtestapp.R
import kotlinx.android.synthetic.main.item_genre.view.*

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
class GenresAdapter : RecyclerView.Adapter<GenresAdapter.ItemHolder>() {

    val data = ArrayList<GenresEntity>()

    fun addGenres(genres: List<GenresEntity>) {
        data.addAll(genres)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_genre, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(data[position])
    }

    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(genre: GenresEntity) = with(itemView) {
            tvTitle.text = genre.name
        }

    }
}