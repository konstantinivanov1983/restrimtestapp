package com.konstantiniiv.restrimtestapp.di.module

import com.konstantiniiv.restrimtestapp.di.scope.PerFragment
import com.konstantiniiv.restrimtestapp.ui.moviedetails.MovieDetailsFragment
import com.konstantiniiv.restrimtestapp.ui.movielist.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Module
abstract class FragmentModule {

    @PerFragment
    @ContributesAndroidInjector()
    abstract fun movieListFragment(): MovieListFragment

    @PerFragment
    @ContributesAndroidInjector()
    abstract fun movieDetailsFragment(): MovieDetailsFragment
}