package com.konstantiniiv.restrimtestapp.di.module

import com.konstantiniiv.data.api.Api
import com.konstantiniiv.data.cache.MemoryMovieCache
import com.konstantiniiv.data.mappers.MovieDataToMovieEntityMapper
import com.konstantiniiv.data.repositories.CachedMoviesDataStore
import com.konstantiniiv.data.repositories.MovieRepositoryImpl
import com.konstantiniiv.data.repositories.RemoteMoviesDataStore
import com.konstantiniiv.domain.cache.MoviesCache
import com.konstantiniiv.domain.datastore.MoviesDataStore
import com.konstantiniiv.domain.repository.MovieRepository
import com.konstantiniiv.restrimtestapp.di.DI
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
@Module
class DataModule {

    @Singleton
    @Provides
    fun provideMovieRepo(api: Api, @Named(DI.inMemoryCache) cache: MoviesCache,
                         movieMapper: MovieDataToMovieEntityMapper): MovieRepository {
        return MovieRepositoryImpl(api = api, cache = cache,
                movieDataToMovieEntityMapper = movieMapper)
    }

    @Singleton
    @Provides
    @Named(DI.inMemoryCache)
    fun provideInMemoryMovieCache(): MoviesCache {
        return MemoryMovieCache()
    }

    @Singleton
    @Provides
    @Named(DI.remoteDataStore)
    fun provideRemoteMovieDataStore(api: Api, movieMapper: MovieDataToMovieEntityMapper)
            : MoviesDataStore {
        return RemoteMoviesDataStore(api = api, movieMapper = movieMapper)
    }

    @Singleton
    @Provides
    @Named(DI.cachedDataStore)
    fun provideCachedMoviesDataStore(moviesCache: MoviesCache): MoviesDataStore {
        return CachedMoviesDataStore(moviesCache = moviesCache)
    }

}