package com.konstantiniiv.data.mappers

import com.konstantiniiv.data.entities.GenresData
import com.konstantiniiv.data.entities.MovieData
import com.konstantiniiv.domain.enteties.GenresEntity
import com.konstantiniiv.domain.enteties.MovieEntity
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieDataToMovieEntityMapper @Inject constructor(private val genreMapper: GenresDataToEntityMapper)
    : Mapper<MovieData, MovieEntity>() {

    override fun map(from: MovieData): MovieEntity {
        var genreEntities: List<GenresEntity>? = null
        from.genre_ids?.let { list: List<GenresData> ->
            genreEntities = genreMapper.map(list)
        }
        if (genreEntities == null) {
            from.genres?.let { list: List<GenresData> ->
                genreEntities = genreMapper.map(list)
            }
        }
        return MovieEntity(id = from.id, releaseDAte = from.release_date,
                posterPath = from.poster_path, title = from.title, voteAverage = from.vote_average,
                originalLanguage = from.original_language, originalTitle = from.original_title,
                overview = from.overview, popularity = from.popularity, video = from.video,
                voteCount = from.vote_count, genres = genreEntities)
    }

    override fun reverseMap(from: MovieEntity): MovieData {
        throw UnsupportedOperationException("not implemented")
    }
}