package com.konstantiniiv.restrimtestapp.di.module

import com.konstantiniiv.restrimtestapp.di.scope.PerActivity
import com.konstantiniiv.restrimtestapp.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Module
abstract class ActivityModule{

    @PerActivity
    @ContributesAndroidInjector
    abstract fun mainActivity() : MainActivity

}