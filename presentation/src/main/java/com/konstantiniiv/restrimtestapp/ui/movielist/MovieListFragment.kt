package com.konstantiniiv.restrimtestapp.ui.movielist

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.restrimtestapp.R
import com.konstantiniiv.restrimtestapp.ui.global.*
import kotlinx.android.synthetic.main.fragment_movies_list.*
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieListFragment : BaseFragment(), BackButtonListener {

    @Inject
    lateinit var fabric: ViewModelProvider.Factory
    lateinit var viewModel: MovieListViewModel
    private var adapter: MovieListAdapter? = null

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, fabric)
                .get(MovieListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        viewModel.init()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.observeStates().subscribe { proceedViewStates(it) }
    }

    override fun onBackClick(): Boolean {
        viewModel.onBackPressed()
        return true
    }

    override fun onDestroy() {
        adapter = null
        super.onDestroy()
    }

    private fun initRecyclerView() {
        rvMovies.layoutManager = GridLayoutManager(activity, 2)
        rvMovies.addItemDecoration(SpacesItemDecoration(spaces = 32, includeEdge = true, spancount = 2))
        if (adapter == null) adapter = MovieListAdapter({ viewModel.onMovieClicked(it) })
        rvMovies.adapter = adapter
    }

    private fun proceedViewStates(state: ViewState<List<MovieEntity>>?) {
        when (state) {
            is ViewState.Success -> adapter?.addMovies(state.data)
            is ViewState.Error -> activity?.toast(state.error)
        }
    }

}