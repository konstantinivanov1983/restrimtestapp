package com.konstantiniiv.restrimtestapp.ui.moviedetails

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.konstantiniiv.domain.usecase.GetMovieUseCase
import ru.terrakok.cicerone.Router

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieDetailsViewModelFactory(private val router: Router,
                                   private val getMovieUseCase: GetMovieUseCase)
    : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailsViewModel(router = router, getMovieUseCase = getMovieUseCase) as T
    }
}