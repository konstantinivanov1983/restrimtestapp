package com.konstantiniiv.restrimtestapp.di.module

import android.content.Context
import com.konstantiniiv.restrimtestapp.App
import com.konstantiniiv.restrimtestapp.di.qualifier.ApplicationContext
import dagger.Binds
import dagger.Module

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Module
abstract class AppModule {

    @Binds
    @ApplicationContext
    internal abstract fun application(app: App) : Context
}