package com.konstantiniiv.data.api

import com.konstantiniiv.data.entities.MovieData

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
data class MovieListResponse(var page: Int = 0,
                             var total_pages: Int = 0,
                             var total_results: Int = 0,
                             var results: List<MovieData> = emptyList()
)