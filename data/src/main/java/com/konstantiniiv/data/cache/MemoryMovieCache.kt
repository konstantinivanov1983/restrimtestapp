package com.konstantiniiv.data.cache

import com.konstantiniiv.domain.cache.MoviesCache
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MemoryMovieCache : MoviesCache {

    private val moviesStored: LinkedHashMap<Int, MovieEntity> = LinkedHashMap()

    override fun clear() {
        moviesStored.clear()
    }

    override fun saveAll(movies: List<MovieEntity>) {
        movies.forEach { movie -> moviesStored[movie.id] = movie }
    }

    override fun getAll(): Observable<List<MovieEntity>> {
        return Observable.just(moviesStored.values.toList())
    }

    override fun get(movieId: Int): Observable<Optional<MovieEntity>> {
        return Observable.just(Optional.of(moviesStored[movieId]))
    }

    override fun isEmpty(): Observable<Boolean> {
        return Observable.fromCallable { moviesStored.isEmpty() }
    }
}