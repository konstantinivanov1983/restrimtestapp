package com.konstantiniiv.restrimtestapp.ui.moviedetails

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.restrimtestapp.R
import com.konstantiniiv.restrimtestapp.ui.global.*
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_movie.*
import javax.inject.Inject

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieDetailsFragment : BaseFragment(), BackButtonListener {

    companion object {

        const val KEY_ID = "key_id"

        fun newInstance(movieId: Int): MovieDetailsFragment {
            val fragment = MovieDetailsFragment()
            val bundle = Bundle()
            bundle.putInt(KEY_ID, movieId)
            fragment.arguments = bundle
            return fragment
        }
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var viewModel: MovieDetailsViewModel
    private var adapter: GenresAdapter? = null

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, factory)
                .get(MovieDetailsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.getMovie(arguments?.getInt(KEY_ID) ?: 0)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.observeStates().subscribe { proceedViewStates(it) }
    }

    override fun onBackClick(): Boolean {
        viewModel.onBackPressed()
        return true
    }

    override fun onDestroy() {
        adapter = null
        super.onDestroy()
    }

    private fun initRecyclerView() {
        rvGenres?.layoutManager = GridLayoutManager(activity, 2)
        rvGenres?.addItemDecoration(SpacesItemDecoration(spancount = 2, includeEdge = false,
                spaces = 16))
        if (adapter == null) adapter = GenresAdapter()
        rvGenres?.adapter = adapter
    }

    private fun proceedViewStates(state: ViewState<MovieEntity>?) {
        when (state) {
            is ViewState.Success -> {
                setVideo(state.data.video)
                setTitleAndDescription(state.data)
                adapter?.let { genresAdapter: GenresAdapter
                    ->
                    genresAdapter.addGenres(state.data.genres ?: emptyList())
                }
            }
            is ViewState.Error -> activity?.toast(state.error)
        }
    }

    private fun setTitleAndDescription(movie: MovieEntity) {
        tvTitle?.text = movie.title
        tvDescription?.text = movie.overview ?: "Нет описания"
    }

    private fun setVideo(path: String?) {
        if (path != null) {
            try {
                vTrailer.setVideoPath(path)
                val mc = MediaController(activity)
                mc.setAnchorView(vTrailer)
                vTrailer.setMediaController(null)
                vTrailer.requestFocus()
                vTrailer.setOnPreparedListener { _ -> vTrailer.start() }
            } catch (e: Exception) {
                Logger.e("Error playing video: " + e.message)
            }
        }
    }
}