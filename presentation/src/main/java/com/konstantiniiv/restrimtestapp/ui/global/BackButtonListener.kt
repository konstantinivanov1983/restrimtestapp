package com.konstantiniiv.restrimtestapp.ui.global

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
interface BackButtonListener{

    fun onBackClick() : Boolean
}