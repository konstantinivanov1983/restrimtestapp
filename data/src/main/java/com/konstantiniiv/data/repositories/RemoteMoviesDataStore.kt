package com.konstantiniiv.data.repositories

import com.konstantiniiv.data.api.Api
import com.konstantiniiv.data.entities.MovieData
import com.konstantiniiv.data.mappers.Mapper
import com.konstantiniiv.domain.datastore.MoviesDataStore
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class RemoteMoviesDataStore(private val api: Api,
                            private val movieMapper: Mapper<MovieData, MovieEntity>)
    : MoviesDataStore {

    //To Implement
    override fun getMovieById(movieId: Int): Observable<Optional<MovieEntity>> {
        throw UnsupportedOperationException("not implemented, no proper GET method!!")
    }

    override fun getMovieList(): Observable<List<MovieEntity>> {
        return api.getMovieList()
                .map { moviesList ->
                    moviesList.results
                            .map { movie -> movieMapper.map(movie) }
                }
    }
}
