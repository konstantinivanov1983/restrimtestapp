package com.konstantiniiv.restrimtestapp.ui.global

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
sealed class ViewState<out T : Any> {

    data class Success<out T : Any>(val data: T) : ViewState<T>()
    data class Error(val error: String) : ViewState<Nothing>()
    data class Message(val message: String) : ViewState<Nothing>()
    object Loading : ViewState<Nothing>()

}
