package com.konstantiniiv.domain.repository

import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
interface MovieRepository {

    fun getMovie(id : Int) : Observable<Optional<MovieEntity>>
    fun getMovieList() : Observable<List<MovieEntity>>

}