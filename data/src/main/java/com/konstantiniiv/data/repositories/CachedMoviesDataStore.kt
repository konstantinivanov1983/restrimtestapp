package com.konstantiniiv.data.repositories

import com.konstantiniiv.domain.cache.MoviesCache
import com.konstantiniiv.domain.datastore.MoviesDataStore
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class CachedMoviesDataStore(private val moviesCache: MoviesCache) : MoviesDataStore {

    override fun getMovieById(movieId: Int): Observable<Optional<MovieEntity>> {
        return moviesCache.get(movieId)
    }

    override fun getMovieList(): Observable<List<MovieEntity>> {
        return moviesCache.getAll()
    }
}