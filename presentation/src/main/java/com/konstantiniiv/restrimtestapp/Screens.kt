package com.konstantiniiv.restrimtestapp

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class Screens{



    companion object {

        const val FRAGMENT_MOVIES_LIST = "fragment_movies_list"
        const val FRAGMENT_MOVIE = "fragment_movie"

    }
}