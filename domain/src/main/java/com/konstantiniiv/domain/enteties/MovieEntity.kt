package com.konstantiniiv.domain.enteties

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
data class MovieEntity(var id: Int,
                       var originalLanguage: String? = null,
                       var originalTitle: String? = null,
                       var overview: String? = null,
                       var releaseDAte: String,
                       var posterPath: String,
                       var popularity: Double? = null,
                       var title: String,
                       var video: String? = null,
                       var voteAverage: Double? = null,
                       var voteCount: Int? = null,
                       var genres: List<GenresEntity>? = null
)