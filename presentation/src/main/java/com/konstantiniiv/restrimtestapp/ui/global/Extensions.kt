package com.konstantiniiv.restrimtestapp.ui.global

import android.app.Activity
import android.widget.Toast
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
fun Activity.toast(message : String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Disposable.disposedBy(bag: CompositeDisposable) {
    bag.add(this)
}

fun Throwable.errorMessage() : String {
    return this.message ?: this.localizedMessage
}