package com.konstantiniiv.data.api

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
interface Api {

    @GET("numbata/5ed307d7953c3f7e716f/raw/b7887adc444188d8aa8e61d39b82950f28c03966/movies.json")
    fun getMovieList(): Observable<MovieListResponse>

}