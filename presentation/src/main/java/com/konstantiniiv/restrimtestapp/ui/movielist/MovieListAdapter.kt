package com.konstantiniiv.restrimtestapp.ui.movielist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.restrimtestapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie_list.view.*
import java.util.*

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieListAdapter(private val onMovieSelected: (Int) -> Unit)
    : RecyclerView.Adapter<MovieListAdapter.ItemHolder>() {

    val data = ArrayList<MovieEntity>()

    fun addMovies(movies: List<MovieEntity>) {
        data.addAll(movies)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie_list, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(movie = data[position], listener = onMovieSelected)
    }

    class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: MovieEntity, listener: (Int) -> Unit) = with(itemView) {
            Picasso.get()
                    .load(movie.posterPath)
                    .into(ivPoster)
            tvTitle.text = String.format(Locale.ENGLISH, "%s  (%s)", movie.title,
                    getYear(movie.releaseDAte))
            setOnClickListener { listener(movie.id) }
        }

        private fun getYear(year: String?): String {
            if (year != null && year.length >= 4) {
                return year.substring(0, 4)
            }
            return ""
        }
    }


}