package com.konstantiniiv.restrimtestapp.di

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
class DI{

    companion object {
        const val inMemoryCache = "inMemoryCache"
        const val remoteDataStore = "remoteDataStore"
        const val cachedDataStore = "cachedDataStore"
    }
}