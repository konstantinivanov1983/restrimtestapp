package com.konstantiniiv.restrimtestapp.di.qualifier

import javax.inject.Qualifier

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext