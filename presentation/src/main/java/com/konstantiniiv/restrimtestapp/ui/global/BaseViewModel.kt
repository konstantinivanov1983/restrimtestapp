package com.konstantiniiv.restrimtestapp.ui.global

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
open class BaseViewModel: ViewModel(){

    val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
    }
}