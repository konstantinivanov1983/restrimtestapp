package com.konstantiniiv.data.entities

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
data class MovieData(
        var id: Int = -1,
        var adult: Boolean = false,
        var original_language: String = "",
        var original_title: String = "",
        var overview: String = "",
        var release_date: String = "",
        var poster_path: String = "",
        var popularity: Double = 0.0,
        var title: String = "",
        var video: String = "",
        var vote_average: Double = 0.0,
        var vote_count: Int = 0,
        var genres: List<GenresData>? = null,
        var genre_ids: List<GenresData>? = null
)