package com.konstantiniiv.data.repositories

import com.konstantiniiv.data.api.Api
import com.konstantiniiv.data.mappers.MovieDataToMovieEntityMapper
import com.konstantiniiv.domain.cache.MoviesCache
import com.konstantiniiv.domain.datastore.MoviesDataStore
import com.konstantiniiv.domain.enteties.MovieEntity
import com.konstantiniiv.domain.enteties.Optional
import com.konstantiniiv.domain.repository.MovieRepository
import io.reactivex.Observable

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 10.07.2018.
 */
class MovieRepositoryImpl(api: Api,
                          private val cache: MoviesCache,
                          movieDataToMovieEntityMapper: MovieDataToMovieEntityMapper)
    : MovieRepository {

    private val memoryDataStore: MoviesDataStore
    private val remoteDataStore: MoviesDataStore

    init {
        memoryDataStore = CachedMoviesDataStore(cache)
        remoteDataStore = RemoteMoviesDataStore(api = api,
                movieMapper = movieDataToMovieEntityMapper)
    }

    override fun getMovie(id: Int): Observable<Optional<MovieEntity>> {
        return cache.isEmpty().flatMap { empty ->
            if (!empty) {
                return@flatMap memoryDataStore.getMovieById(movieId = id)
            } else {

                //Предполагается запрос из сети, при пустом кеше
                return@flatMap Observable.just(Optional(null))
            }
        }
    }

    override fun getMovieList(): Observable<List<MovieEntity>> {
        return cache.isEmpty().flatMap { empty ->
            if (!empty) {
                return@flatMap memoryDataStore.getMovieList()
            } else {
                return@flatMap remoteDataStore.getMovieList().doOnNext { cache.saveAll(it) }
            }
        }
    }
}