package com.konstantiniiv.restrimtestapp.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.konstantiniiv.data.api.Api
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
@Module
class ApiModule{

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Singleton
    @Provides
    fun provideGson() : Gson {
        return GsonBuilder()
                .setLenient()
                .create()
    }


    @Singleton
    @Provides
    fun provideApi(retrofit: Retrofit) : Api = retrofit.create(Api::class.java)

    @Singleton
    @Provides
    fun provideHttpClient() : OkHttpClient {
        return OkHttpClient.Builder()
                .build()
    }

}