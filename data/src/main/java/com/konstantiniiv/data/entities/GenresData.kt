package com.konstantiniiv.data.entities

/**
 * Created by Konstantin Ivanov
 * email :  ki@agileburo.com
 * on 11.07.2018.
 */
data class GenresData(val name: String = "")